# SimpleUniqueCities

simple console app to dedupe two files with cities that may have specific culture info.


# how to use: 

Clone repository

go to root directory of repository ./simpleuniquecities
if you have current .net cli installed run the command 'dotnet build'
because this is a simple solution and not a project we need to run the dll with dotnet to run this.
CD to ./simpleuniquecities/ConsoleApp1/bin/Debug/netcoreapp2.2
then run 'dotnet .\ConsoleApp1.dll'
this will create 2 files in this same directory. These will be generated every time the application is run.