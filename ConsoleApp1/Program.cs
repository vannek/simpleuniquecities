﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var patha = Directory.GetCurrentDirectory() + @"\ItalyFileA.TXT";
                var pathb = Directory.GetCurrentDirectory() + @"\ItalyFileB.TXT";
                
                string texta = File.ReadAllText(patha);
                string textb = File.ReadAllText(pathb);

                //can only split on a single char so we have to take out half of the \r\n for newlines
                texta = texta.Replace("\r", "");
                textb = textb.Replace("\r", "");
                //make our two lists to comapre
                var citiesa = texta.Split('\n').ToList();
                var citiesb = textb.Split('\n').ToList();
                //set up our list of matches
                List<string> matches = new List<string>();
                //collect all of our matches
                foreach (var city in citiesa)
                {
                    if (citiesb.Contains(city, StringComparer.InvariantCultureIgnoreCase))
                    {
                        matches.Add(city);
                    }
                }
                //remove the matches from both lists to leave unique items in a and b
                foreach(var match in matches)
                {
                    citiesa.RemoveAll(x => x == match);
                    citiesb.RemoveAll(x => x == match);
                }
                

                Console.WriteLine("writing out first list of unique cities");
                WriteFile(Directory.GetCurrentDirectory() + @"\ItalyUniqueInA.txt", citiesa);
                Console.WriteLine("writing out second list of unique cities");
                WriteFile(Directory.GetCurrentDirectory() + @"\ItalyUniqueInB.txt", citiesb);
                Console.WriteLine("Done!");
            }
            //specific error for IO.Directory use. Need to be able to read/write
            catch(UnauthorizedAccessException uae)
            {
                Console.WriteLine("Not authorized to access this directory. Please move project or run with elevated privileges");
                Console.WriteLine(uae.Message);
            }
            //specific error for version mismatch, need to have .NET
            catch(NotSupportedException nse)
            {
                Console.WriteLine("This operation is not supported. Please install a compatible .NET framework");
                Console.WriteLine(nse.Message);
            }
            //Catch all exception 
            catch (Exception ex)
            {
                if(ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                    Console.WriteLine(ex.InnerException.StackTrace);
                }
                else
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }
        //reduced redundant code by moving out shared file logic write to a single method
        static void WriteFile(string filePath, List<string> itemsToWrite)
        {
            //always use try catch. We're gonna throw if anything happens so our program can catch it at the top level
            try
            {
                //if the files are already there delete them and create new ones.
                if(File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                using (var sw = new StreamWriter(File.Open(filePath, FileMode.Create), Encoding.UTF8))
                {
                    foreach (var city in itemsToWrite)
                    {
                        sw.WriteLine(city);
                    }
                }
            } catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
